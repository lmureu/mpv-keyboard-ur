local kb = libs.keyboard;


-- Documentation
-- http://www.unifiedremote.com/api

-- Keyboard Library
-- http://www.unifiedremote.com/api/libs/keyboard


--@help Command 1
-- actions.command1 = function ()
-- 	kb.stroke("ctrl", "alt", "delete");
-- end

actions.exit = function()
	kb.stroke("q")
end

actions.open = function()
	kb.stroke("command", "down")
end

actions.info = function()
	kb.stroke("o")
end

actions.fullscreen = function()
	kb.stroke("f")
end

actions.subtrack = function()
	kb.stroke("j")
end

actions.inc_brightness = function()
	kb.stroke("4")
end

actions.dec_brightness = function()
	kb.stroke("3")
end

actions.inc_volume = function()
	kb.stroke("*")
end

actions.dec_volume = function()
	kb.stroke("/")
end

actions.play = function()
	kb.stroke("space")
end

actions.up = function()
	kb.stroke("up")
end

actions.down = function()
	kb.stroke("down")
end

actions.right = function()
	kb.stroke("right")
end

actions.left = function()
	kb.stroke("left")
end
